declare global {
    let PROJECT_ID: string
    let PROJECT_API_KEY: string
    let PROJECT_URL: string
    let AUTH_TOKEN: string
    let request: any
    let CURRENT_WORKFLOW: {
        id: string
        name: string
        description: string
        createdAt: string
        updatedAt: string
        totalProcessingTime: string
    }
    let JOB: {
        id: string
    }
}
export { }