async function globalSetup() {
  const config = resolveSelectedConfiguration() || {}
}

//If we ever need to read a configuration file before the tests
//This is for environment configurations
function resolveSelectedConfiguration() {
  // const { configurations } = require('./config.js')
  // const configName = process.env.CONFIGURATION
  // return configurations[configName]
}

module.exports = globalSetup
