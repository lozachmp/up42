export default class Utils {
    //sleeps in seconds
    static async sleep(seconds: number): Promise<unknown> {
        return new Promise(resolve => setTimeout(resolve, seconds * 1000))
    }
}
