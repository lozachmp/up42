//const _ = require('lodash/core')

import Utils from "@libs/utils"
import JsonResponses from '@qa/schemas.json'
import { matchers } from 'jest-json-schema'
expect.extend(matchers)
import { StatusCodes } from 'http-status-codes'

//linked test cases: create, add, run, monitor
describe(`Create a new workflow consisting of the subsequent blocks`, function () {
  it(`/POST - ${StatusCodes.OK} - Create new workflow`, async () => {
    /** Create new workflow test
     * @tag Create
     * @severity critical
     */
    await request
      .post(`/projects/${PROJECT_ID}/workflows/`)
      .set('Content-type', 'application/json')
      .set('Authorization', `Bearer ${AUTH_TOKEN}`)
      .send({
        "name": "QA coding challenge workflow",
        "description": "Workflow description"
      })
      .expect(StatusCodes.OK)
      .expect('Content-Type', /json/)
      .then((response: any) => {
        CURRENT_WORKFLOW = response.body.data
        expect(response.body).toMatchSchema(JsonResponses.workflowJsonSchema)
      })
  })

  it(`/POST - ${StatusCodes.OK} - Adding tasks to the workflow`, async () => {
    /** Adding blocks to the workflow: MODIS (GeoTIFF) by NASA and Sharpening Filter by UP42
     * @severity critical
     */
    await request
      .post(`/projects/${PROJECT_ID}/workflows/${CURRENT_WORKFLOW.id}/tasks/`)
      .set('Content-type', 'application/json')
      .set('Authorization', `Bearer ${AUTH_TOKEN}`)
      .send([{
        "name": "nasa-modis:1",
        "parentName": null,
        "blockId": "ef6faaf5-8182-4986-bce4-4f811d2745e5"
      },
      {
        "name": "sharpening:1",
        "parentName": "nasa-modis:1",
        "blockId": "e374ea64-dc3b-4500-bb4b-974260fb203e"
      }])
      .expect(StatusCodes.OK)
      .expect('Content-Type', /json/)
      .then((response: any) => {
        expect(response.body).toMatchSchema(JsonResponses.tasksJsonSchema)
      })
  })
})

describe(`Create and run a job based on the created workflow`, function () {
  //Test with 10000ms timeout
  it(`/POST - ${StatusCodes.OK} - Create new job`, async () => {
    /** Create new job
     * @severity critical
     */
    await request
      .post(`/projects/${PROJECT_ID}/workflows/${CURRENT_WORKFLOW.id}/jobs/`)
      .set('Content-type', 'application/json')
      .set('Authorization', `Bearer ${AUTH_TOKEN}`)
      .send({
        "nasa-modis:1": {
          "time": "2018-12-01T00:00:00+00:00/2020-12-31T23:59:59+00:00",
          "limit": 1,
          "zoom_level": 9,
          "imagery_layers": [
            "MODIS_Terra_CorrectedReflectance_TrueColor"
          ],
          "bbox": [
            13.365373,
            52.49582,
            13.385796,
            52.510455
          ]
        },
        "sharpening:1": {
          "strength": "medium"
        }
      })
      .expect(StatusCodes.OK)
      .expect('Content-Type', /json/)
      .then((response: any) => {
        JOB = response.body.data
        expect(response.body).toMatchSchema(JsonResponses.jobJsonSchema)
      })
  }, 10000)

  //Test with 244000ms timeout
  it(`/GET - ${StatusCodes.OK} - Monitor the job`, async () => {
    /** Monitor the job every 30 seconds during 4 minutes
     * @severity critical
     */
    let current_jobStatus = undefined
    while (current_jobStatus != "SUCCEEDED") {
      await request
        .get(`/projects/${PROJECT_ID}/jobs/${JOB.id}`)
        .set('Authorization', `Bearer ${AUTH_TOKEN}`)
        .expect(StatusCodes.OK)
        .expect('Content-Type', /json/)
        .then((response: any) => {
          current_jobStatus = response.body.data.status
          switch (current_jobStatus) {
            case "PENDING": {
              expect(response.body).toMatchSchema(JsonResponses.jobPENDINGStatusJsonSchema)
              break
            }
            case "RUNNING": {
              expect(response.body).toMatchSchema(JsonResponses.jobRUNNINGStatusJsonSchema)
              break
            }
            case "SUCCEEDED": {
              expect(response.body).toMatchSchema(JsonResponses.jobSUCCEEDEDJsonSchema)
              break
            }
            case "NOT_STARTED": {
              expect(response.body).toMatchSchema(JsonResponses.jobNOT_STARTEDJsonSchema)
              break
            }
            default: {
              //should not happen so we exit
              console.error(`current_jobStatus: ${current_jobStatus}`)
              expect(true).toBeFalsy()
            }
          }
        })
      //we wait 30 secs between checks
      await Utils.sleep(30)
    }
  }, 244000)
})

describe(`Delete the created workflow`, function () {
  it(`/DELETE - ${StatusCodes.NO_CONTENT} - Delete the workflow`, async () => {
    /** Delete the created workflow
     * @severity critical
     */
    await request
      .delete(`/projects/${PROJECT_ID}/workflows/${CURRENT_WORKFLOW.id}`)
      .set('Authorization', `Bearer ${AUTH_TOKEN}`)
      .expect(StatusCodes.NO_CONTENT)
      .then((response: any) => {
        //no content response
        expect(response.body).toMatchSchema({})
      })
  })
  it(`/GET - ${StatusCodes.OK} - Check the workflow is deleted`, async () => {
    /** Check if the workflow has been deleted
     * @severity normal
     */
    await request
      .get(`/projects/${PROJECT_ID}/workflows`)
      .set('Authorization', `Bearer ${AUTH_TOKEN}`)
      .then((response: any) => {
        //list of workflow is empty
        expect(expect.arrayContaining(response.body.data)).toEqual([])
      })
  })
})