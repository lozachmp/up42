const supertest = require('supertest')

//Project configuration
PROJECT_ID = process.env.PROJECT_ID || ''
PROJECT_API_KEY = process.env.PROJECT_API_KEY || ''
PROJECT_URL = 'https://api.up42.com'
//Init the requester
request = supertest(PROJECT_URL)

// First step when launching tests: we get the token to run authentified tests
beforeAll(async () => {
  await request
    .post('/oauth/token')
    .auth(PROJECT_ID, PROJECT_API_KEY)
    .set('Content-type', 'application/x-www-form-urlencoded')
    .send('grant_type=client_credentials')
    .then((response: any) => {
      AUTH_TOKEN = response.body.access_token
    })
    .catch((err: Error) => {
      console.error(err.message)
      process.exit()
    })
})

// For test dev purposes only:
// We erase all workflows to clean up account (when issue while developing tests)
// Careful not to be activated while running other tests!
// afterAll(async () => {
//   let workflowsJson: any[] = []
//   //Get list of all workflows for current account
//   await request
//     .get(`/projects/${PROJECT_ID}/workflows`)
//     .set('Content-type', 'application/json')
//     .set('Authorization', `Bearer ${AUTH_TOKEN}`)
//     .then((response: any) => {
//       workflowsJson = response.body.data.map((element: any) => {
//         return {
//           id: element.id
//         }
//       })
//     })
//   //Delete all existing workflows for current account
//   for (const workflow of workflowsJson) {
//     await request
//       .delete(`/projects/${PROJECT_ID}/workflows/${workflow.id}`)
//       .set('Authorization', `Bearer ${AUTH_TOKEN}`)
//   }
// })