## Dependencies

1. install [node.js](https://nodejs.org/en/)
2. install [yarn](https://classic.yarnpkg.com/en/docs/install#mac-stable)
3. install [java](https://www.java.com/en/download/help/download_options.html) (only for html reporting)

## Run the tests locally

1. Clone this repository  `git clone https://gitlab.com/lozachmp/up42.git`
2. Change the path `cd up42`
3. Install the project `yarn`
4. Run the tests locally: `yarn test`
5. Check the html test report: `yarn report`

## Run the tests in the cloud

1. Go to the [pipeline area of this repo](https://gitlab.com/lozachmp/up42/-/pipelines/new) 
2. Click on ***Run pipelines*** to run the default pipeline

(allure report artifacts can be generated on the runner but would need to be published on gitlab pages or on a s3 bucket)